import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GreeterComponent } from './greeter.component';

describe('GreeterComponent', () => {
  let component: GreeterComponent;
  let fixture: ComponentFixture<GreeterComponent>;

  /**
   * beforeEach se volá 2x, první volání je pro templates (templateUrl)
   * Protože nčítání templates je time consumming, je voláno asynchronně
   */
  /*
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GreeterComponent ]
    })
    .compileComponents(); // je nutné volat metodu asynchroně
    // webpack zabaluje komponenty do js, proto není potřebné volat compileComponents
  }));
  */

  beforeEach(() => {
    // přesunuto z beforeEach(async())
    TestBed.configureTestingModule({
      declarations: [ GreeterComponent ]
    });

    fixture = TestBed.createComponent(GreeterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
