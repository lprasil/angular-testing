import { TodosComponent } from './todos.component';
import { TodoService } from './todo.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/throw';

describe('TodosComponent', () => {
  let component: TodosComponent;
  let service: TodoService;
  beforeEach(() => {
    service = new TodoService(null);
    component = new TodosComponent(service);
  });

  it('set todos property with the items returned from service', () => {
     const todos = [
      {id: 1, title: 'title1'},
      {id: 1, title: 'title1'},
      {id: 1, title: 'title1'}
    ];
     spyOn(service, 'getTodos').and  // check if the method was calledm get control over the method that class
     .callFake(() => { // getTodos přepíše getTodos a nahradí backend, getTodos vrací Observable
     return Observable.from([todos]); // simulate the response
    });
    component.ngOnInit();
    expect(component.todos).toBe(todos);
  });
  it('should call the server to save the changes when a new todo item is added', () => {
    const spy = spyOn(service, 'add').and
    .callFake(t => {
      return Observable.empty();
    });
    component.add();
    expect(spy).toHaveBeenCalled();
  });
  it('should set the message property if server return an error when adding a new todo', () => {
    const todo = { id: 1 };
    const spy = spyOn(service, 'add').and
    .returnValue(Observable.from([todo])); // 1. varianta
    /*.callFake(t => {                         // 2. varianta
      return Observable.from([ todo ]);
    });*/
    component.add();
    expect(component.todos.length).toBe(1);
  });
  it('should add the new todo returned from server', () => {
    const error = 'error from the server';
    const spy = spyOn(service, 'add').and
    .returnValue(Observable.throw(error)); // 1. varianta

    component.add();
    expect(component.message).toBe(error);
  });
  // remove
  it('it should call the servet to delete a todo item if the user confirms', () => {
    spyOn(window, 'confirm').and.returnValue(true);
    const spy = spyOn(service, 'delete').and.returnValue(Observable.empty());
    component.delete(1);
    expect(spy).toHaveBeenCalledWith(1);
  });
  it('it should NOT call the servet to delete a todo item if the user cancels', () => {
    spyOn(window, 'confirm').and.returnValue(false);
    const spy = spyOn(service, 'delete').and.returnValue(Observable.empty());
    component.delete(1);
    expect(spy).not.toHaveBeenCalled();
  });
});
