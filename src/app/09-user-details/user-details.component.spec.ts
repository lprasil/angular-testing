import { Observable, Subject } from 'rxjs';
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { UserDetailsComponent } from './user-details.component';
import { Router, ActivatedRoute } from '@angular/router';
/**
 * Implementuj Dummy lighway pro router
 * Nahradíme Router za RouterStub a ActivatedRouterStub ktery se importuje v UserDetailsComponent
 */
class RouterStub {
  navigate(params) {

  }
}
class ActivatedRouteStub {
  private subject = new Subject();
  push(value) {
    this.subject.next(value);
  }
  // potřebujeme přidat ID parametr do params, místo Observable
  // použijeme get
  get params() { // public property
    return this.subject.asObservable();
  }
  // params: Observable<any> = Observable.empty(); //public field
}
describe('UserDetailsComponent', () => {
  let component: UserDetailsComponent;
  let fixture: ComponentFixture<UserDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDetailsComponent ],
      providers: [
        { provide: Router, useClass: RouterStub },
        { provide: ActivatedRoute, useClass: ActivatedRouteStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should redirect user to users page after saving', () => {
    const router = TestBed.get(Router);
    const spy = spyOn(router, 'navigate');
    component.save();
    expect(spy).toHaveBeenCalledWith(['users']);
  });
  // Dealing with Route params
  it('should navigate to not-found page if the invalit id passed', () => {
    const router = TestBed.get(Router);
    const spy = spyOn(router, 'navigate');
    const route: ActivatedRouteStub = TestBed.get(ActivatedRoute);
    // route.params. params je Observable, nemužeme do něj poslat parametr
    route.push({id: 0});
    expect(spy).toHaveBeenCalledWith(['not-found']);
  });
  
});
