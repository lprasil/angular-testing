import { compute } from './compute';
// first argumet = name of the test, second is function to test
// function {} nebo arrou function
describe('compute', () => {
    it('should return 0 if input is negative', () => {
        const result = compute(-1);
        expect(result).toBe(0); // jasmine, Karma hlídá testy
    });
    it('should increment if is positive', () => {
        const result = compute(1);
        expect(result).toBe(2);
    });
});
