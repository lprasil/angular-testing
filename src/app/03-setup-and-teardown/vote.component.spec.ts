import { VoteComponent } from './vote.component';

describe('VoteComponent', () => {
  let component: VoteComponent;
  /**
   * create new instance before each test
   *
   * set up
   */
  beforeEach(() => {
    component = new VoteComponent();
  });

  /**
   * tear down
   */
  afterEach(() => {

  });

  it('increase number of total vote by 1', () => {
    component.upVote();
    expect(component.totalVotes).toBe(1);

    component.upVote();
    expect(component.totalVotes).toBe(2);

    component.upVote();
    expect(component.totalVotes).toBe(3);
  });

  it('decrease nummber of total vote by 1', () => {
    component.downVote();
    expect(component.totalVotes).toBe(-1);
  });
});
