import { By } from '@angular/platform-browser';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { VoterComponent } from './voter.component';
/**
 * Místo vztvoření instance VoterComponent, musíme volat TestBed
 * deklarace je stejná jako v app.module.ts
 * @NgModule({
 *   declarations: [
 *
 */
describe('VoterComponent', () => {
  let component: VoterComponent;
  let fixture: ComponentFixture<VoterComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VoterComponent]
    }); // create dynamic testing module
    fixture = TestBed.createComponent(VoterComponent);
    component = fixture.componentInstance;
    // fixture.debugElement // used for DOM
  });
  /*
  Potřebujeme otestovat jestli se ukazuje totalVotes,
  jestli se změní po upVotes, a class bude highlighted
  <div class="voter">
    <i class="glyphicon glyphicon-menu-up vote-button"
          [class.highlighted]="myVote == 1"
          (click)="upVote()"></i>
      <span class="vote-count">{{ totalVotes }}</span>
  */
  it('should render total votes', () => {
    component.myVote = 1;
    component.othersVote = 20;
    fixture.detectChanges();
    // pro totalCount hledáme DOM objekt v HTML
    const de = fixture.debugElement.query(By.css('.vote-count'));
    // fixture.debugElement.queryAll(By.css('vote-count'));
    const el: HTMLElement = de.nativeElement;
    expect(el.innerText).toContain('21');
  });
  it('it should highlight the button if I upvoted', () => {
    component.myVote = 1;
    fixture.detectChanges();
    const de = fixture.debugElement.query(By.css('.glyphicon-menu-up'));
    expect(de.classes['highlighted']).toBeTruthy();
  });
  it('should increase total votes when I clicked upvote button', () => {
    const button = fixture.debugElement.query(By.css('.glyphicon-menu-up'));
    button.triggerEventHandler('click', null);
    expect(component.totalVotes).toBe(1);
  });
});
