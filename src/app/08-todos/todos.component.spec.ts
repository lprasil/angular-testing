// import { Observable } from 'rxjs';
// misto 'rxjs/Observable'; importujeme celou knihovno, jinak by jsem museli importovat rxjs/add/Observable/..
import { Observable } from 'rxjs/Observable';

/* tslint:disable:no-unused-variable */
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TodosComponent } from './todos.component';
import { TodoService } from './todo.service';
import { HttpModule } from '@angular/http';

// NOTE: I've deliberately excluded this suite from running
// because the test will fail. This is because we have not
// provided the TodoService as a dependency to TodosComponent.
//
// When you get to Lecture 6 (Providing Dependencies), be sure
// to remove "x" from "xdescribe" below.

describe('TodosComponent', () => {
  let component: TodosComponent;
  let fixture: ComponentFixture<TodosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      declarations: [ TodosComponent ],
      providers: [TodoService]
      // pro Unit test jsme volaly new TodoService() a spyOn()
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodosComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  /**
   * v Component service se volá OnInit automaticky, pro test ne.
   * Proto musíme načást komponenty zvlášť
   */
  /*
  it('should load components from the serve Observable', () => {
    const todos = [1, 2, 3];
    const service = TestBed.get(TodoService);
    // provide reference to service which in invludet in component
    // vrati dependeci registrovanou na module levelu, ne app.module.ts
    spyOn(service, 'getTodos').and.returnValue(Observable.from([todos])); // nezapomenout na []  !!!
    fixture.detectChanges();
    // ... volá angular NgOnInit, tzn se vzrvoří instance pro Service (druhá se už netvoří)
    // musíme zavolat detect changes až po spyOn
    // jinak vzhazuje error at Object.<anonymous>  ...
    expect(component.todos).toBe(todos);
    console.log('EXPECT WAS CALLED');
  });
  */
  // Promise 1. metoda s async
  it('should load components from the serve Promise', async( () => {
    const todos = [1, 2, 3];
    const service = TestBed.get(TodoService);
    spyOn(service, 'getTodosPromise').and.returnValue(Promise.resolve(todos));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.todos).toBe(todos);
      console.log('EXPECT WAS CALLED');
    });
    // whenStable Promise sync, continue when all asyn component operations are completed
  }));
  // Promise 2. metoda s fakeAsync a tick
  it('should load components from the serve Promise', fakeAsync( () => {
      const todos = [1, 2, 3];
      const service = TestBed.get(TodoService);
      spyOn(service, 'getTodosPromise').and.returnValue(Promise.resolve(todos));
      fixture.detectChanges();
      tick();
      expect(component.todos).toBe(todos);
      console.log('EXPECT WAS CALLED');
      // whenStable Promise sync, continue when all asyn component operations are completed
    }));
});
