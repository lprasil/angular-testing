# AngularTesting

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Run `export CHROME_BIN=chromium-browser` to fix
> No binary for Chrome browser on your platform. Please, set "CHROME_BIN" env variable.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Unit Testing Limitations

Problem to test Routers
Template bindings, we are not sure if the component render this to view

## Code Coverage

we need to know how much is the project covarage with test
`ng test --code-coverage` we get new folder coverage

- Statements
- Brancheches = how much of if/else
- Functions
- Lines (of code)